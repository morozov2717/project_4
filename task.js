function func(myArray){
    myArray.sort();
    console.log('Обычная функция JS: ', myArray);
}
func([3,5,2,1,4])

const myFunc = (array) =>{
    array.sort();
    console.log('Стрелочная функция JS: ',array);
}
myFunc([3,5,2,1,4])